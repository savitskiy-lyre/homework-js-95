const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const {nanoid} = require("nanoid");

const SALT_WORK_FACTOR = 10;

const validateUnique = async value => {
  const user = await User.findOne({email: value});
  if (user) return false;
};

const validateEmail = value => {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
};

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Username or password is incorrect !?'],
  },
  password: {
    type: String,
    required: [true, 'Username or password is incorrect !?'],
  },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: [
        {validator: validateEmail, message: 'Email is not valid!'},
        {validator: validateUnique, message: 'This user is already registered!'}
      ],
    },
  image: {
    type: String,
  },
  token: {
    type: String,
    required: [true, 'Ooops token is gone'],
  },
  facebookId: String,
  googleId: String,
  role: {
    type: String,
    required: true,
    enum: {
      values: ['user', 'admin'],
      message: '{VALUE} is not supported',
    },
    default: 'user',
  },
})


UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  this.password = await bcrypt.hash(this.password, salt);

  next();
});

UserSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.password;
    return ret;
  },
});

UserSchema.methods.generateToken = function () {
  this.token = nanoid();
}

UserSchema.methods.checkPassword = function (password) {
  if (!password) return false;
  return bcrypt.compare(password, this.password);
};

const User = mongoose.model('users', UserSchema);
module.exports = User;