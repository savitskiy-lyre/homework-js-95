const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const IngredientSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name of ingredient is required"],
  },
  amount: {
    type: String,
    default: 'unknown',
  }
},);

const CocktailSchema = new mongoose.Schema({
  author: {
    _id : {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'Author id is required'],
    },
    username: {
      type: String,
      required: true,
    },
  },
  title: {
    type: String,
    required: [true, 'Title required'],
  },
  description: {
    type: String,
    required: [true, 'Description required'],
  },
  image: {
    type: String,
  },
  published: {
    type: Boolean,
    required: true,
    default: false,
  },
  ingredients: [IngredientSchema],
})

CocktailSchema.plugin(idValidator);
const Cocktail = mongoose.model('cocktails', CocktailSchema);
module.exports = Cocktail;