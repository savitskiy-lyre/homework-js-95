const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const authUser = require('../middleware/authUser');
const User = require('../models/User');
const {nanoid} = require("nanoid");
const config = require("../config");
const {OAuth2Client} = require('google-auth-library')
const axios = require("axios");
const clientGoogle = new OAuth2Client(process.env.GOOGLE_CLIENT_ID)

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.post('/', upload.single('image'), async (req, res) => {
  const {username, password, email} = req.body;
  const preUserData = {username, password, email};
  if (req.file) preUserData.image = '/uploads/' + req.file.filename;
  const user = new User(preUserData);

  try {
    user.generateToken();
    await user.save();
    res.send(user);

  } catch (err) {
    if (err?.errors) return res.status(400).send(err.errors);
    res.status(500).send({global: 'Server error, please try again !'});
  }
});

router.post('/facebookLogin', async (req, res) => {

  const inputToken = req.body.accessToken;
  const accessToken = config.facebook.appId + "|" + config.facebook.appSecret;
  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
  try {
    const {data} = await axios.get(debugTokenUrl);
    if (data.data.error) return res.status(401).send({global: 'Facebook token incorrect!'});
    if (req.body.id !== data.data.user_id) return res.status(401).send({global: 'Wrong user id'});

    let user = await User.findOne({email: req.body.email});

    if (!user) {
      user = await User.findOne({facebookId: req.body.id});
    }

    if (!user) {
      user = new User({
        email: req.body.email || nanoid(),
        password: nanoid(),
        facebookId: req.body.id,
        username: req.body.name,
        image: req.body.picture.data.url || null,
      });
    }

    user.generateToken();
    user.save({validateBeforeSave: false});

    res.send(user);
  } catch (err) {
    res.status(401).send({error: 'Facebook token incorrect!'})
  }
});


router.post('/googleLogin', async (req, res) => {
  const {token} = req.body;
  try {
    const ticket = await clientGoogle.verifyIdToken({
      idToken: token,
      audience: process.env.GOOGLE_CLIENT_ID,
    });
    const {name, email, picture, sub: googleId} = ticket.getPayload();

    let user = await User.findOne({email});

    if (!user) {
      user = new User({
        email,
        username: name,
        password: nanoid(),
        image: picture || null,
        googleId,
      })
    }

    user.generateToken();
    await user.save({validateBeforeSave: false});

    res.send(user);
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again !'});
  }
});

router.post('/sessions', authUser, async (req, res) => {
  try {
    req.user.generateToken();
    await req.user.save({validateBeforeSave: false});
    res.send(req.user);
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again !'});
  }

});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Not really Success'};
  if (!token) return res.send({message: 'No token Success'});
  const user = await User.findOne({token});
  if (!user) return res.send(success);
  try {
    user.generateToken();
    await user.save({validateBeforeSave: false});
    return res.send({message: 'Success'});
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again !'});
  }

});

module.exports = router;