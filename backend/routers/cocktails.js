const express = require('express');
const authUserToken = require("../middleware/authUserToken");
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const permit = require("../middleware/permit");
const router = express.Router();
const Cocktail = require('../models/Cocktail');
const User = require("../models/User");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  const reqToken = req.get('Authorization');

  try {
    let user;
    if (reqToken) {
      user = await User.findOne({token: reqToken});
    }
    let cocktails;
    if (user?.role === 'admin'){
      cocktails = await Cocktail.find();
    } else {
      cocktails = await Cocktail.find({published: {$in: true}});
    }
    res.send(cocktails);
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again later!'});
  }
});


router.get('/self', authUserToken, async (req, res) => {
  try {
    const cocktails = await Cocktail.find({author: {_id: req.user._id, username: req.user.username}});
    res.send(cocktails);
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again later!'});
  }
});

router.post('/publish/:id', authUserToken, permit('admin'), async (req, res) => {
  const id = req.params.id;
  if (!id) return  res.status(400).send('Cocktail id required');
  try {
    const cocktail = await Cocktail.findById(id);
    if (!cocktail) return  res.status(404).send('Cocktail not found');
    cocktail.published = !cocktail.published;
    cocktail.save();
    res.send({global: cocktail.published ? 'Cocktail published !': 'Cocktail unpublished !'});
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again later!'});
  }
});

router.delete('/:id', authUserToken, permit('admin'), async (req, res) => {
  const id = req.params.id;
  if (!id) return  res.status(400).send('Cocktail id required');
  try {
    const cocktail = await Cocktail.findById(id);
    if (!cocktail) return res.status(404).send('Cocktail not found');
    await Cocktail.findByIdAndDelete(id);
    res.send({global: 'Deleted successfully'});
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again later!'});
  }
});

router.post('/', authUserToken, upload.single('image'), async (req, res) => {
  const {title, description, ingredients: jsonIngredients} = req.body;
  const ingredients = JSON.parse(jsonIngredients);
  const preCocktail = {title, description, author: {_id: req.user._id, username: req.user.username}};
  if (req.file) preCocktail.image = '/uploads/' + req.file.filename;
  preCocktail.ingredients = ingredients;

  const cocktail = new Cocktail(preCocktail);
  try {
    await cocktail.save();
    res.send(cocktail);
  } catch (err) {
    res.status(400).send({global: 'Server error, please try again later!'});
  }
});


module.exports = router;