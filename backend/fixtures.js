const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");


const run = async () => {
    await mongoose.connect(config.db.testStorageUrl)

    const collections = await mongoose.connection.db.listCollections().toArray();
    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [user1, user2] = await User.create({
        username: 'tester',
        password: 'test',
        email: 'user@good.com',
        image: '/fixtures/user.svg',
        token: nanoid(),
    }, {
        username: 'admin',
        password: 'admin',
        email: 'admin@good.com',
        image: '/fixtures/moder.jpg',
        role: 'admin',
        token: nanoid(),
    })

    await mongoose.connection.close();
}
run().catch(console.error)
