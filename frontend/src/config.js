export const BASE_URL = 'http://localhost:9000';
export const USERS_URL = '/users';
export const COCKTAIL_URL = '/cocktails';
export const OWN_COCKTAIL_URL = '/cocktails/self';
export const PUBLISH_COCKTAIL_URL = '/cocktails/publish/';
export const DELETE_COCKTAIL_URL = '/cocktails/';
export const USERS_SESSIONS_URL = '/users/sessions';
export const GOOGLE_LOGIN = '/users/googleLogin';
export const USER_FC_LOGIN_URL = '/users/facebookLogin';
export const IMAGES_URL = BASE_URL + '/';

export const facebookAppId = process.env.REACT_APP_FACEBOOK_APP_ID;
