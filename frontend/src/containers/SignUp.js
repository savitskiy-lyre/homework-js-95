import React, {useCallback, useEffect} from 'react';
import Box from "@mui/material/Box";
import {useDispatch, useSelector} from "react-redux";
import FormSignUp from "../components/UI/Form/FormSignUp";
import {registerUser, registerUserFailure} from "../store/actions/usersActions";

const SignUp = () => {
    const errAccount = useSelector((state) => state.profile.errAccount);
    const registerLoading = useSelector((state) => state.profile.registerLoading);
    const dispatch = useDispatch();
    const resetErrHandler = useCallback((key) => {
        dispatch(registerUserFailure({...errAccount, [key]: null}));
    }, [errAccount, dispatch])

    useEffect(() => () => {
        dispatch(registerUserFailure(null))
    }, [dispatch]);

    return (
        <Box justifyContent={"center"} width={'100%'} pt={8}>
            <FormSignUp
                actionName='Sign up'
                errors={errAccount}
                resetErrHandler={resetErrHandler}
                helperLinkName='Already have an account? Sign in'
                toLocation='/signin'
                loading={registerLoading}
                onSubmit={(data) => dispatch(registerUser(data))}
            />
        </Box>
    );
};

export default SignUp;