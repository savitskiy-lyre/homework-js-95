import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {useSelector} from "react-redux";
import {Stack} from "@mui/material";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import {IMAGES_URL} from "../../../config";
import CardContent from "@mui/material/CardContent";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '400',
  maxHeight: '80vh',
  overflow: 'auto',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 1,
};

const OwnCocktailsModalBtn = () => {
  const ownCocktails = useSelector(state => state.cocktails.ownCocktails);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Button onClick={handleOpen}
              variant={"contained"}
      >
        My cocktails
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Stack spacing={1}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Your cocktails:
            </Typography>
            {ownCocktails?.length > 0 && (
              ownCocktails.map((cocktail) => {
                return (
                  <Card key={cocktail._id + 'own'}>
                    <CardMedia
                      component="img"
                      height="240"
                      image={IMAGES_URL + cocktail.image}
                      alt={cocktail.title}
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="div">
                        {cocktail.title}
                      </Typography>
                      <Typography variant="body2" color="text.secondary">
                        {cocktail.description}
                      </Typography>
                    </CardContent>
                  </Card>
                )
              })
            )}
          </Stack>
        </Box>
      </Modal>
    </div>
  );
};


export default OwnCocktailsModalBtn;