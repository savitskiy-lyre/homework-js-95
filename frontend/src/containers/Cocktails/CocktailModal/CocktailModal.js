import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {CardMedia, Stack} from "@mui/material";
import Grid from "@mui/material/Grid";
import {IMAGES_URL} from "../../../config";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  minWidth: 400,
  maxHeight: '80vh',
  overflow: 'auto',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
};

const CocktailModal = ({closeModal, isModalOpen, currentCocktail}) => {
  const {author, description: recipe, image, ingredients, title, _id} = currentCocktail;
  return (
    <div>
      <Modal
        open={isModalOpen}
        onClose={closeModal}
      >
        <Box sx={style}>
          <CardMedia
            component="img"
            height={380}
            image={IMAGES_URL + image}
          />
          <Stack p={2} spacing={2}>
            <Typography variant={"h3"} textAlign={"center"}>
              {title}
            </Typography>
            <Grid container flexWrap={"nowrap"}>
              <Grid item xs={3}>
                <Stack spacing={2}>
                  <Typography variant={"h5"} textAlign={"center"}>
                    Author:
                  </Typography>
                  <Typography variant={"h6"} textAlign={"center"}>
                    {author.username}
                  </Typography>
                </Stack>
              </Grid>
              <Grid item xs={9}>
                <Stack spacing={1}>
                  <Typography variant={"h5"} textAlign={"center"}>
                    Ingredients:
                  </Typography>
                  <Grid container justifyContent={"space-evenly"} spacing={1}>
                    {ingredients.map((ingredient) => {
                      return (
                        <Grid item key={_id + ingredient.name}>
                          <Typography variant={"body2"}>
                            <strong>Name: </strong> {ingredient.name}
                          </Typography>
                          <Typography variant={"body2"}>
                            <strong>Amount: </strong> {ingredient.amount}
                          </Typography>
                        </Grid>
                      );
                    })}
                  </Grid>
                </Stack>
              </Grid>
            </Grid>
            <Typography variant={"h5"} textAlign={"center"}>
              Recipe:
            </Typography>
            <Typography variant={"body2"}>
              {recipe}
            </Typography>
          </Stack>
        </Box>
      </Modal>
    </div>
  );
};

export default CocktailModal;