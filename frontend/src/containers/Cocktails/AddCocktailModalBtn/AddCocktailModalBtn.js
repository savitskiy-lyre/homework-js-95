import * as React from 'react';
import {useState} from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {Avatar, Stack, TextField} from "@mui/material";
import LocalBarIcon from '@mui/icons-material/LocalBar';
import {pink} from "@mui/material/colors";
import Grid from "@mui/material/Grid";
import PlusOneIcon from '@mui/icons-material/PlusOne';
import RemoveOutlinedIcon from '@mui/icons-material/RemoveOutlined';
import {addCocktail} from "../../../store/actions/cocktailsActions";
import {useDispatch, useSelector} from "react-redux";
import {LoadingButton} from "@mui/lab";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  minWidth: 500,
  maxHeight: '80vh',
  overflow: 'auto',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 1,
};


const ingredientRow = {name: '', amount: ''};
const initCocktail = {
  title: '',
  description: '',
  image: null,
}
const someId = Math.random();

const AddCocktailModalBtn = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [ingredients, setIngredients] = useState([ingredientRow]);
  const [cocktailState, setCocktailState] = useState(initCocktail)
  const loading = useSelector(state => state.cocktails.loadingAdd);

  const onCocktailSubmit = (e) => {
    e.preventDefault();
    const newCocktailData = {...cocktailState, ingredients: JSON.stringify(ingredients)};
    const cocktailFormData = new FormData();
    Object.keys(newCocktailData).forEach((key) => {
      cocktailFormData.append(key, newCocktailData[key]);
    })
    dispatch(addCocktail(cocktailFormData));
    setIngredients([ingredientRow]);
    setCocktailState(initCocktail);
  };

  const addIngredientChanger = () => {
    setIngredients(prevState => [...prevState, ingredientRow]);
  };
  const removeIngredientChanger = () => {
    if (ingredients.length <= 1) return;
    setIngredients(prevState => {
      const copyState = [...prevState];
      copyState.pop();
      return copyState;
    });
  };

  const ingredientChanger = (event, i) => {
    setIngredients(prev => {
      const copyState = [...prev];
      copyState[i] = {...prev[i], [event.target.name]: event.target.value};
      return copyState;
    });
  };

  const inpImageChanger = (e) => {
    setCocktailState(prev => ({...prev, image: e.target.files[0]}));
  }

  const inpCocktailChanger = (e) => {
    setCocktailState(prev => ({...prev, [e.target.name]: e.target.value}));
  }

  return (
    <div>
      <Button onClick={handleOpen} variant={"contained"}>Add</Button>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Box sx={style} textAlign={"center"}>
          <Stack component={'form'} onSubmit={onCocktailSubmit} spacing={2} my={1} px={1}>
            <Avatar sx={{bgcolor: pink[500], padding: '25px', margin: '0 auto'}}>
              <LocalBarIcon fontSize={"large"}/>
            </Avatar>
            <Typography variant="h4" component="h2">
              Add new cocktail
            </Typography>
            <TextField
              type={"text"}
              name={'title'}
              label={"Name"}
              onChange={inpCocktailChanger}
              value={cocktailState.title}
            />
            <TextField
              type={"text"}
              multiline
              rows={4}
              label={"Recipe"}
              name={'description'}
              onChange={inpCocktailChanger}
              value={cocktailState.description}
            />
            <Typography variant={"h5"}>
              Ingredients:
            </Typography>
            {ingredients.map((ing, i) => {
              return (
                <Grid container flexWrap={"nowrap"} justifyContent={"space-between"} key={someId + i}>
                  <Grid item>
                    <TextField
                      type={"text"}
                      name={'name'}
                      onChange={(e) => {
                        ingredientChanger(e, i)
                      }}
                      value={ingredients[i].name}
                      label={"Name"}
                    />
                  </Grid>
                  <Grid item ml={1}>
                    <TextField
                      type={"text"}
                      name={'amount'}
                      label={"Amount"}
                      onChange={(e) => {
                        ingredientChanger(e, i)
                      }}
                      value={ingredients[i].amount}
                    />
                  </Grid>
                </Grid>
              );
            })}
            <Grid container>
              <Grid item xs={9}>
                <Button
                  fullWidth
                  onClick={addIngredientChanger}
                >
                  <PlusOneIcon/>
                </Button>
              </Grid>
              <Grid item xs={3}>
                <Button
                  color={"error"}
                  onClick={removeIngredientChanger}
                  fullWidth
                >
                  <RemoveOutlinedIcon/>
                </Button>
              </Grid>
            </Grid>
            <TextField
              type={"file"}
              name={'image'}
              onChange={inpImageChanger}
            />
            <LoadingButton variant={'contained'}
                           loading={loading}
                           type={'submit'}
            >
              Submit
            </LoadingButton>
          </Stack>
        </Box>
      </Modal>
    </div>
  );
};
export default AddCocktailModalBtn;