import React, {useEffect, useState} from 'react';
import {makeStyles} from "@mui/styles";
import {Button, Stack, Typography} from "@mui/material";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import ActionAreaCard from "../../components/UI/ActionAreaCard/ActionAreaCard";
import {useDispatch, useSelector} from "react-redux";
import CocktailModal from "./CocktailModal/CocktailModal";
import {
  deleteCocktail,
  fetchCocktails,
  fetchOwnCocktails,
  publishCocktail,
  setCurrentCocktail
} from "../../store/actions/cocktailsActions";
import AddCocktailModalBtn from "./AddCocktailModalBtn/AddCocktailModalBtn";
import OwnCocktailsModalBtn from "./OwnCocktailsModalBtn/OwnCocktailsModalBtn";

const useStyles = makeStyles({
  mainWrapper: {
    border: '2px solid gainsboro',
    borderRadius: '3px',
    padding: '5px',
    minHeight: '80vh',
  },
  bottomDivider: {
    borderBottom: '1px solid gainsboro',
  },
  minCardWidth: {
    minWidth: 250,
  },
});


const Cocktails = () => {
  const c = useStyles();
  const dispatch = useDispatch();
  const cocktails = useSelector(state => state.cocktails.data);
  const currentCocktail = useSelector(state => state.cocktails.currentCocktail);
  const profile = useSelector(state => state.profile.data);

  const [isCocktailModalOpen, setIsCocktailModalOpen] = useState(false);
  const openCocktailModal = (_id) => {
    dispatch(setCurrentCocktail(_id));
    setIsCocktailModalOpen(true);
  };
  const closeCocktailModal = () => setIsCocktailModalOpen(false);

  useEffect(() => {
    dispatch(fetchCocktails());
    dispatch(fetchOwnCocktails());
  }, [dispatch])

  return (
    <Stack className={c.mainWrapper} my={3}>
      <Box m={1}>
        <Grid container justifyContent={"space-between"} spacing={1} py={1} className={c.bottomDivider}>
          <Grid item>
            <Typography variant={'h5'}>
              Cocktails:
            </Typography>
          </Grid>
          <Grid item>
            <Grid container spacing={1}>
              <Grid item>
                <AddCocktailModalBtn/>
              </Grid>
              <Grid item>
                <OwnCocktailsModalBtn/>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
      <Grid container
            spacing={1}
            justifyContent={"center"}
            justifyItems={"center"}
            flexGrow={1}
            alignItems={"center"}
            alignContent={"flex-start"}
            flexWrap={"wrap"}
            p={1}
      >
        {cocktails?.length > 0 && (
          cocktails.map((cocktail) => {
            return (
              <Grid item xs={4} className={c.minCardWidth} key={cocktail._id}>
                <ActionAreaCard
                  title={cocktail.title}
                  image={cocktail.image}
                  description={cocktail.description}
                  openModal={() => openCocktailModal(cocktail._id)}
                >
                  {profile?.role === 'admin' && (
                    <Grid container spacing={1} justifyContent={"center"} p={1}>
                      <Grid item>
                        <Button variant={"contained"}
                                color={cocktail.published ? 'success' : 'warning'}
                                onClick={() => dispatch(publishCocktail(cocktail._id))}
                        >
                          {cocktail.published ? 'unPublish' : 'Publish'}
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button variant={"contained"}
                                color={"error"}
                                onClick={() => dispatch(deleteCocktail(cocktail._id))}
                        >
                          Delete
                        </Button>
                      </Grid>
                    </Grid>
                  )}
                </ActionAreaCard>
              </Grid>
            )
          })
        )
        }
        {
          currentCocktail && (
            <CocktailModal
              closeModal={closeCocktailModal}
              isModalOpen={isCocktailModalOpen}
              currentCocktail={currentCocktail}
            />
          )
        }

      </Grid>
    </Stack>
  );
};

export default Cocktails;