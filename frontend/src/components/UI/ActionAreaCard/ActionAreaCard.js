import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {CardActionArea} from '@mui/material';
import {IMAGES_URL} from "../../../config";

const ActionAreaCard = ({title, image, description, openModal, children}) => (
  <Card>
    <CardActionArea onClick={openModal}>
      <CardMedia
        component="img"
        height="240"
        image={IMAGES_URL + image}
        alt={title}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
      </CardContent>
    </CardActionArea>
    {children}
  </Card>
);

export default ActionAreaCard;