import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {COCKTAIL_URL, DELETE_COCKTAIL_URL, OWN_COCKTAIL_URL, PUBLISH_COCKTAIL_URL} from "../../config";
import {
  addCocktail,
  addCocktailFailure,
  addCocktailSuccess,
  deleteCocktail, deleteCocktailFailure, deleteCocktailSuccess,
  fetchCocktails,
  fetchCocktailsFailure,
  fetchCocktailsSuccess, fetchOwnCocktails, fetchOwnCocktailsFailure, fetchOwnCocktailsSuccess,
  publishCocktail, publishCocktailFailure, publishCocktailSuccess
} from "../actions/cocktailsActions";

export function* fetchCocktailsSaga() {
  try {
    const {data} = yield axiosApi.get(COCKTAIL_URL);
    yield put(fetchCocktailsSuccess(data));
  } catch (err) {
    if (!err.response) {
      toast.error(err.message);
    } else {
      toast.error(err.response?.data?.global);
    }
    yield put(fetchCocktailsFailure(err.response?.data));
  }
}

export function* fetchOwnCocktailsSaga() {
  try {
    const {data} = yield axiosApi.get(OWN_COCKTAIL_URL);
    yield put(fetchOwnCocktailsSuccess(data));
  } catch (err) {
    if (!err.response) {
      toast.error(err.message);
    }
    yield put(fetchOwnCocktailsFailure(err.response?.data));
  }
}

export function* addCocktailSaga({payload: newCocktail}) {
  try {
    yield axiosApi.post(COCKTAIL_URL, newCocktail);
    yield put(addCocktailSuccess());
    toast.success('Your cocktail has been created and is being reviewed by the moderator');
  } catch (err) {
    if (!err.response) {
      toast.error(err.message);
    } else {
      toast.error(err.response?.data?.global);
    }
    yield put(addCocktailFailure(err.response?.data));
  }
}

export function* publishCocktailSaga({payload: id}) {
  try {
    const {data} = yield axiosApi.post(PUBLISH_COCKTAIL_URL + id);
    yield put(fetchCocktails());
    yield put(publishCocktailSuccess());
    toast.success(data?.global);
  } catch (err) {
    if (!err.response) {
      toast.error(err.message);
    } else {
      toast.error(err.response?.data?.global);
    }
    yield put(publishCocktailFailure(err.response?.data));
  }
}

export function* deleteCocktailSaga({payload: id}) {
  try {
    yield axiosApi.delete(DELETE_COCKTAIL_URL + id);
    yield put(fetchCocktails());
    yield put(deleteCocktailSuccess());
    toast.success('Deleted');
  } catch (err) {
    if (!err.response) {
      toast.error(err.message);
    } else {
      toast.error(err.response?.data?.global);
    }
    yield put(deleteCocktailFailure(err.response?.data));
  }
}

const cocktailsSaga = [
  takeEvery(fetchOwnCocktails, fetchOwnCocktailsSaga),
  takeEvery(fetchCocktails, fetchCocktailsSaga),
  takeEvery(addCocktail, addCocktailSaga),
  takeEvery(publishCocktail, publishCocktailSaga),
  takeEvery(deleteCocktail, deleteCocktailSaga),
];

export default cocktailsSaga;
