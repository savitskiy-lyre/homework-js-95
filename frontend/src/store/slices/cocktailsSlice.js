import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  data: null,
  ownCocktails: null,
  currentCocktail: null,
  loadingAdd: false,
  loadingPublish: false,
  loadingDelete: false,
  errDelete: null,
  errFetch: null,
  errPublish: null,
  errAdd: null,
};
const name = 'cocktails';

const cocktailsSlice = createSlice({
  name,
  initialState,
  reducers: {
    fetchCocktails(state) {
      state.errFetch = null;
    },
    fetchCocktailsSuccess(state, {payload}) {
      state.data = payload;
    },
    fetchCocktailsFailure(state, {payload}) {
      state.errFetch = payload;
    },
    fetchOwnCocktails(state) {
      state.errFetch = null;
    },
    fetchOwnCocktailsSuccess(state, {payload}) {
      state.ownCocktails = payload;
    },
    fetchOwnCocktailsFailure(state, {payload}) {
      state.errFetch = payload;
    },
    addCocktail(state) {
      state.loadingAdd = true;
      state.errAdd = null;
    },
    addCocktailSuccess(state) {
      state.loadingAdd = false;
    },
    addCocktailFailure(state, {payload}) {
      state.loadingAdd = false;
      state.errAdd = payload;
    },
    publishCocktail(state) {
      state.loadingPublish = true;
      state.errPublish = null;
    },
    publishCocktailSuccess(state) {
      state.loadingPublish = false;
    },
    publishCocktailFailure(state, {payload}) {
      state.loadingPublish = false;
      state.errPublish = payload;
    },
    deleteCocktail(state) {
      state.loadingDelete = true;
      state.errDelete = null;
    },
    deleteCocktailSuccess(state) {
      state.loadingDelete = false;
    },
    deleteCocktailFailure(state, {payload}) {
      state.loadingDelete = false;
      state.errDelete = payload;
    },
    setCurrentCocktail(state, {payload: _id}) {
      state.currentCocktail = state.data.find((cocktail) => {
        return cocktail._id === _id;
      })
    },
  }
});

export default cocktailsSlice;
