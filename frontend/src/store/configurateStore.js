import {combineReducers} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import createSagaMiddleware from 'redux-saga';
import {configureStore} from "@reduxjs/toolkit";
import usersSlice from "./slices/usersSlice";
import {rootSagas} from "./rootSagas";
import cocktailsSlice from "./slices/cocktailsSlice";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  profile: usersSlice.reducer,
  cocktails: cocktailsSlice.reducer,
});
const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
  thunk,
  sagaMiddleware,
];

const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: true,
  preloadedState: persistedState
});

store.subscribe(() => {
  saveToLocalStorage({
    profile: store.getState().profile,
  });
})

sagaMiddleware.run(rootSagas);

export default store;