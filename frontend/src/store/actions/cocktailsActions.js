import cocktailsSlice from "../slices/cocktailsSlice";

export const {
  fetchCocktails,
  fetchCocktailsSuccess,
  fetchCocktailsFailure,
  fetchOwnCocktails,
  fetchOwnCocktailsSuccess,
  fetchOwnCocktailsFailure,
  addCocktail,
  addCocktailSuccess,
  addCocktailFailure,
  publishCocktail,
  publishCocktailSuccess,
  publishCocktailFailure,
  deleteCocktail,
  deleteCocktailSuccess,
  deleteCocktailFailure,
  setCurrentCocktail,
} = cocktailsSlice.actions;
